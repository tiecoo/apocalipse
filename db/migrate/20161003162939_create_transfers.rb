class CreateTransfers < ActiveRecord::Migration[5.0]
  def change
    create_table :transfers do |t|
      t.integer :peple1
      t.integer :item1
      t.integer :quantity1
      t.integer :peple2
      t.integer :item2
      t.integer :quantity2

      t.timestamps
    end
  end
end

class CreateInventories < ActiveRecord::Migration[5.0]
  def change
    create_table :inventories do |t|
      t.references :peple, foreign_key: true
      t.references :item, foreign_key: true
      t.integer :quantity
      t.integer :points

      t.timestamps
    end
  end
end

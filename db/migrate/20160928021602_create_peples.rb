class CreatePeples < ActiveRecord::Migration[5.0]
  def change
    create_table :peples do |t|
      t.string :name
      t.string :gender
      t.boolean :infected
      t.integer :age
      t.float :longitude
      t.float :latitude

      t.timestamps
    end
  end
end

class PepleSerializer < ActiveModel::Serializer
  attributes :id, :name, :gender, :infected, :age, :longitude, :latitude
end

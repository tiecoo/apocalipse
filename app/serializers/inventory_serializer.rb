class InventorySerializer < ActiveModel::Serializer
  attributes :id, :quantity, :points
  has_one :peple
  has_one :item
end

class Api::V1::TransfersController < ApplicationController
  before_action :set_transfer, only: [:show, :edit, :update, :destroy]
  skip_before_filter  :verify_authenticity_token

  # GET /transfers
  # GET /transfers.json
  def index
    @transfers = Transfer.all
    render :json => @transfers
  end

  # GET /transfers/1
  # GET /transfers/1.json
  def show
  end

  # GET /transfers/new
  def new
    @transfer = Transfer.new
  end

  # GET /transfers/1/edit
  def edit
  end

  # POST /transfers
  # POST /transfers.json
  def create
    @transfer = Transfer.new(transfer_params)

    if is_infected(@transfer.peple1, @transfer.peple2) == false
      render json: { errors: @transfer.errors}, status: 422
    else
      @bag1 = find_bag(@transfer.peple1)
      @bag2 = find_bag(@transfer.peple2)
      @items1 = @bag.where(items_id: @transfer.items1)
      @items2 = @bag.where(items_id: @transfer.items2)
      if @items2.points == @items1.points
         @bag1.peple_id = @transfer.peple2
         @bag1.save
         @bag2.peple_id = @transfer.peple1
         @bag2.save
         @transfer.save
          render json: @transfer, status: 201
      else
         ender json: { errors: @transfer.errors}, status: 422
      end
    end
  end

  # PATCH/PUT /transfers/1
  # PATCH/PUT /transfers/1.json
  def update
      if @transfer.update(transfer_params)
        render json: @transfer, status: 200
      else
        render json: { errors: @transfer.errors }, status: 422
      end
  end

  # DELETE /transfers/1
  # DELETE /transfers/1.json
  def destroy
    @transfer.destroy
    head 204
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transfer
      @transfer = Transfer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transfer_params
      params.require(:transfer).permit(:peple1_id, :item1_id, :quantity1, :peple2_id, :item2_id, :quantity)
    end

    def is_infected( peple1, peple2)
      peple1 = Peple.where(id: peple1)
      peple2 = Peple.where(id: peple2)
      if peple1.infected == true || peple2.infected == true
         return false
      else
         return true
      end
    end

    def find_bag(person)
      id_person = person
      return Inventory.where(peple_id:  id_person)
   end

end

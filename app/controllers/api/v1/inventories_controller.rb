class Api::V1::InventoriesController < ApplicationController
  before_action :set_inventory, only: [:show, :edit, :update, :destroy]
  skip_before_filter  :verify_authenticity_token
  # GET /inventories
  # GET /inventories.json
  def index
    @inventories = Inventory.all
    render :json => @inventories
  end

  # GET /inventories/1
  # GET /inventories/1.json
  def show
  end

  # GET /inventories/new
  def new
    @inventory = Inventory.new
  end

  # GET /inventories/1/edit
  def edit
  end

  # POST /inventories
  # POST /inventories.json
  def create
    @inventory = Inventory.new(inventory_params)

    @inventory.points = @inventory.quantity * @inventory.item.points

    if @inventory.save
      render json: @inventory, status: 201
    else
      render json: { errors: @inventory.errors}, status: 422
    end
  end

  # PATCH/PUT /inventories/1
  # PATCH/PUT /inventories/1.json
  def update
    if @inventory.update(inventory_params)
     render json: @inventory, status: 200
     else
       render json: { errors: @inventory.errors }, status: 422
    end
  end

  # DELETE /inventories/1
  # DELETE /inventories/1.json
  def destroy
    @inventory.destroy
    head 204
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inventory
      @inventory = Inventory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inventory_params
      params.require(:inventory).permit(:peple_id, :item_id, :quantity)
    end
end

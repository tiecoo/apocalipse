class Api::V1::ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]
skip_before_filter  :verify_authenticity_token

  # GET /items
  def index
    @items = Item.all
    render :json => @items
  end

  # GET /items/1
  def show
  end

  # GET /items/new
  def new
    @item = Item.new
  end

  # GET /items/1/edit
  def edit
  end

  # POST /items
  def create
    @item = Item.new(item_params)

    if @item.save
      render json: @item, status: 201
    else
      render json: { errors: @item.errors}, status: 422
    end
  end

  # PATCH/PUT /items/1
  def update
    if @item.update(item_params)
     render json: @item, status: 200
     else
       render json: { errors: @item.errors }, status: 422
    end
  end

  # DELETE /items/1
  def destroy
    @item.destroy
    head 204
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def item_params
      params.require(:item).permit(:name, :points)
    end
end

class Api::V1::PeplesController < ApplicationController
  before_action :set_peple, only: [:show, :edit, :update, :destroy]
skip_before_filter  :verify_authenticity_token
  # GET /peples
  def index
    @peples = Peple.all
    render :json => @peples
  end

  # GET /peples/1
  def show
  end

  # GET /peples/new
  def new
    @peple = Peple.new
  end

  # GET /peples/1/edit
  def edit
  end

  # POST /peples
  def create
    @peple = Peple.new(peple_params)

    if @peple.save
      render json: @peple, status: 201
    else
      render json: { errors: @peple.errors}, status: 422
    end
  end

  # PATCH/PUT /peples/1
  def update
    if @peple.update(peple_params)
      render json: @peple, status: 200
     else
       render json: { errors: @peple.errors }, status: 422
    end
  end

  # DELETE /peples/1
  def destroy
    @peple.destroy
    head 204
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_peple
      @peple = Peple.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def peple_params
      params.require(:peple).permit(:name, :gender, :infected, :age, :longitude, :latitude)
    end
end

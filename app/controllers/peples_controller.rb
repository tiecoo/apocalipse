class PeplesController < ApplicationController
  before_action :set_peple, only: [:show, :edit, :update, :destroy]

  # GET /peples
  def index
    @peples = Peple.all
  end

  # GET /peples/1
  def show
  end

  # GET /peples/new
  def new
    @peple = Peple.new
  end

  # GET /peples/1/edit
  def edit
  end

  # POST /peples
  def create
    @peple = Peple.new(peple_params)

    if @peple.save
      redirect_to @peple, notice: 'Peple was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /peples/1
  def update
    if @peple.update(peple_params)
      redirect_to @peple, notice: 'Peple was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /peples/1
  def destroy
    @peple.destroy
    redirect_to peples_url, notice: 'Peple was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_peple
      @peple = Peple.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def peple_params
      params.require(:peple).permit(:name, :gender, :infected, :age, :longitude, :latitude)
    end
end

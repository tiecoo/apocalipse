json.extract! inventory, :id, :peple_id, :item_id, :quantity, :points, :created_at, :updated_at
json.url inventory_url(inventory, format: :json)
require 'test_helper'

class PeplesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @peple = peples(:one)
  end

  test "should get index" do
    get peples_url
    assert_response :success
  end

  test "should get new" do
    get new_peple_url
    assert_response :success
  end

  test "should create peple" do
    assert_difference('Peple.count') do
      post peples_url, params: { peple: { age: @peple.age, gender: @peple.gender, infected: @peple.infected, latitude: @peple.latitude, longitude: @peple.longitude, name: @peple.name } }
    end

    assert_redirected_to peple_url(Peple.last)
  end

  test "should show peple" do
    get peple_url(@peple)
    assert_response :success
  end

  test "should get edit" do
    get edit_peple_url(@peple)
    assert_response :success
  end

  test "should update peple" do
    patch peple_url(@peple), params: { peple: { age: @peple.age, gender: @peple.gender, infected: @peple.infected, latitude: @peple.latitude, longitude: @peple.longitude, name: @peple.name } }
    assert_redirected_to peple_url(@peple)
  end

  test "should destroy peple" do
    assert_difference('Peple.count', -1) do
      delete peple_url(@peple)
    end

    assert_redirected_to peples_url
  end
end

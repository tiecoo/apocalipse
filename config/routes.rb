Rails.application.routes.draw do
  resources :transfers
  resources :inventories
  resources :items
  resources :peples
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api, defaults: {format: :json} do
   namespace :v1 do
        resources :inventories
        resources :peples
        resources :items
        resources :transfers
   end
  end

end
